2D - Visibility algorithm 
=========================

Aim
---
I wanted to implement an algorithm managing the lights and visiblity algorithms. 

The hidden purpose is to prepare its implementation to another project module : Shelduck.

Technical view
--------------
The LightManager works with its own class Light that contains a color, intensity, position...

The Manager can add and draw a light.

When drawing a light, it looks for the sf::SPRITES the program gave it to know how to draw the ray.

The precision (and then performance) variables are constances in the header file. From now, they're at a satisfaying level.


Testing control
---------------
Arrow to move the sprite

Upcoming
--------
* Add a light with controls
* Add a sprite with controls
* Additive logic in multiple light intersections
* Add transparent, translucid and other light-affecting sprites.