/* LightManager */
#ifndef LIGHTMANAGER_HPP
#define LIGHTMANAGER_HPP
#include <SFML/Graphics.hpp>
#include <memory>
#include <iostream>
#include <Light.hpp>

struct RayTriangle
{
  sf::Vertex origin;
  sf::Vertex newPoint;
  sf::Vertex lastPoint;

  sf::Vertex toVertex( size_t n )
  {
    switch(n)
      {
      case 0: return origin; break;
      case 1: return newPoint; break;
      case 2: return lastPoint; break;
      default : return sf::Vertex(); break;
      }
  }
  
};

class LightManager
{
public:
  LightManager( std::shared_ptr<sf::RenderWindow> window );

  void addLight(float x, float y, sf::Color color = sf::Color(255,255,255), float intensity = 1.0f, float radius = 100.0f);
  void update(float dt);
  void draw(void);

  // BLOCK
  void addBlock(std::shared_ptr<sf::Sprite>& sp, bool moving, BLOCK_STATUS status, sf::Color col = sf::Color::White);
  void addBlock(sf::Sprite* sp, bool moving,  BLOCK_STATUS status, sf::Color col = sf::Color::White);
  void addBlock(sf::Sprite& sp, bool moving,  BLOCK_STATUS status, sf::Color col = sf::Color::White);

  
  virtual ~LightManager();
  
protected:
  void drawTheLigths(void);
  void updateRayTriangle(RayTriangle& triangle, std::shared_ptr<Light>& light);
  void updateRayTriangleFromBlocks(RayTriangle& triangle, std::shared_ptr<Light>& light);
  void colorizeRayTriangle(RayTriangle& triangle, std::shared_ptr<Light>& light);

  // utils for light manager
  sf::Color getColor(std::shared_ptr<Light>& light, float distance);
  sf::Color getColor(std::shared_ptr<Light>& light, float x, float y);
  
  // utils
  bool contains(std::shared_ptr<sf::Sprite>& sp, float x, float y);
  bool containsHalf(std::shared_ptr<sf::Sprite>& sp, float x, float y);
private:
  LightManager( LightManager const& lightmanager ) = delete;
  LightManager& operator=( LightManager const& lightmanager ) = delete;

  std::shared_ptr<sf::RenderWindow> m_window;
  std::vector<std::shared_ptr<Light>> m_lights;
  size_t const m_circlePart = 60;
  size_t const m_rayDivision = 100;
  sf::VertexArray m_triangles;
  static sf::Color const m_minimalColor;
};

#endif
