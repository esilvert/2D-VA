/* Light */
#ifndef LIGHT_HPP
#define LIGHT_HPP
#include <iostream>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <vector>
#include <memory>
class LightManager;


enum class BLOCK_STATUS
{
  TRANSPARENT
    , TEINTED
    , OPAQUE
};

struct Block
{
  Block() = default;
  Block(std::shared_ptr<sf::Sprite> sp, BLOCK_STATUS st, sf::Color col = sf::Color::White) : sprite(sp), status(st), color(col) {}

  std::shared_ptr<sf::Sprite> sprite;
  BLOCK_STATUS status;
  sf::Color color;
};


class Light
{
public:
  Light(LightManager *const manager, float x, float y, float intensity, float radius, sf::Color color);

  void addBlock(std::shared_ptr<sf::Sprite>& sp, bool moving = false, BLOCK_STATUS status = BLOCK_STATUS::OPAQUE, sf::Color col = sf::Color::Black);  
  // getter 
  float         getX()            const {return m_x;}
  float         getY()            const {return m_y;}
  float         getRadius()       const {return m_radius;}
  float         getIntensity()    const {return m_intensity;}
  sf::Color     getColor()        const {return m_color;}
  std::vector<std::shared_ptr<Block>> const getBlocks();
  
  virtual ~Light();
  
protected:
  bool isInRange(std::shared_ptr<sf::Sprite>& sp) const;
  void sort(std::vector<std::shared_ptr<Block>>& vec);
private:
  Light( Light const& light ) = delete;
  Light& operator=( Light const& light ) = delete;
  // we want to receive "this", weak cannot work. Shared make it crash, unique is non sens. then raw is perfect
  LightManager* m_manager;
  float         m_x;
  float         m_y;
  float         m_intensity;
  float         m_radius;
  sf::Color     m_color;
  std::vector<std::shared_ptr<Block>> m_blocks;
  
};

#endif
