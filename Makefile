EXEC=2dVisibility.out
CXX=clang++
CXXFLAGS=-Wall -std=c++11 -I"include/" 
SRC=$(wildcard src/*.cpp)
OBJ=$(SRC:.cpp=.o)

$(EXEC) : $(OBJ)
	$(CXX) $(CXXFLAGS) $^ -lsfml-graphics -lsfml-window -lsfml-system -o $@

clean:
	rm -f src/*.o

mrproper: clean
	rm -f $(EXEC)
