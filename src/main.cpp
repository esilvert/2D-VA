#include <iostream>
#include <SFML/Graphics.hpp>
#include <memory>
#include <LightManager.hpp>
#include <cassert>
#include <chrono>
int main()
{
  std::shared_ptr<sf::RenderWindow> window( new sf::RenderWindow(sf::VideoMode(800,600), "[2D] Visibility Algorithm"));
  std::shared_ptr<LightManager> lm( new LightManager(window) );

  //  window->setFramerateLimit(60);
  
   lm->addLight(400,300, sf::Color::White, 0.5f, 200.0f);
   lm->addLight(600,100, sf::Color::Blue, 1.f, 50.0f);

   //
   std::vector<std::shared_ptr<sf::Sprite>> sprites;
   
   // player
   sf::Texture text; assert(text.loadFromFile("tileset.png"));
   std::shared_ptr<sf::Sprite> sp(new sf::Sprite(text));
   sp->setPosition(100,100);
   sp->setScale(0.2f, 0.2f);
   sprites.push_back(sp);

   // tests
   for ( size_t i(0) ; i < 10 ; ++i )
     {
       std::shared_ptr<sf::Sprite> sp(new sf::Sprite(text));
       sp->setPosition(250 + (i%5)*50, i/5 * 100 + 200);
       sp->setScale(0.2f, 0.2f);
       sprites.push_back(sp);
     }
   //

   for ( auto& sprite : sprites )
     {
       lm->addBlock(sprite, true, BLOCK_STATUS::OPAQUE, sf::Color::Blue);
     }


  while ( window->isOpen() )
    {
      std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
      sf::Event event;
      while( window->pollEvent( event ) )
      {
	switch( event.type )
	  {
	  case sf::Event::Closed :
	    {
	      window->close();
	    }
	    break;
	  case sf::Event::KeyPressed:
	    {
	      switch ( event.key.code )
		{
		default : break;
		case sf::Keyboard::Escape :
		  {
		    window->close();
		  }
		  break;
		}
	    }
	    break;
	  default: break;
	  }
      }
      
      // KeybindingsM 
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::Right) )
	{
	  sp->move(0.5, 0);
	}
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::Left) )
	{
	  sp->move(-0.5, 0);
	}
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::Up) )
	{
	  sp->move(0, -0.5);
	}
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::Down) )
	{
	  sp->move(0, 0.5);
	}
      
      // Update
      window->clear(sf::Color::Black);
      lm->update(0);

      // Draw
   for ( auto& sprite : sprites )
     {
      window->draw(*sprite.get());
     }
      


      lm->draw();
      window->display();

      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      std::chrono::duration<double> dt = end - start;
      window->setTitle("[2D] Visibility Algorithm" + std::to_string(1.f/dt.count()));
    }

  return 0;
}
