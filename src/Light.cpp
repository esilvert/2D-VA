#include <Light.hpp>
#include <LightManager.hpp>
#include <cassert>

Light::Light(LightManager *const manager, float x, float y, float intensity, float radius, sf::Color color) :
  m_manager(manager)
  , m_x(x)
  , m_y(y)
  , m_intensity(intensity)
  , m_radius(radius)
  , m_color(color)
{
  assert(intensity <= 1.0f); // percent of transparency
  assert(m_radius >= 0);
  assert(m_manager != nullptr);
}

// Add a block that can hide the rays
void Light::addBlock(std::shared_ptr<sf::Sprite>& sp, bool moving,  BLOCK_STATUS status, sf::Color col)
{
  // Everything moving can arrive in the light's radius
  if ( moving || isInRange(sp) )
    {
      m_blocks.emplace_back(new Block(sp, status, col));
    }
}

bool Light::isInRange(std::shared_ptr<sf::Sprite>& sp) const
{
  float sp_x = sp->getPosition().x;
  float sp_w = sp->getTextureRect().width * sp->getScale().x;
  float sp_y = sp->getPosition().y;
  float sp_h = sp->getTextureRect().height * sp->getScale().y;
	
  return (fabs(sp_x - m_x) < m_radius + sp_w && fabs(sp_y - m_y) < m_radius + sp_h);
}

// sort the object regarding the x then the y coordinate
void Light::sort( std::vector<std::shared_ptr<Block>>& vec)
{
  // Sprites sprites 
  std::sort( vec.begin(), vec.end(), [&](std::shared_ptr<Block>& s1, std::shared_ptr<Block>& s2){

      float s1_x = s1->sprite->getPosition().x;
      float s1_y = s1->sprite->getPosition().y;
      float s2_x = s2->sprite->getPosition().x;
      float s2_y = s1->sprite->getPosition().y;

      float dx_1 = fabs(s1_x - m_x);
      float dy_1 = fabs(s1_y - m_y);
      float dx_2 = fabs(s2_x - m_x);
      float dy_2 = fabs(s2_y - m_y);
			
      
      return (
	      (dx_1 == dx_2)
	      ? dy_1 < dy_2
	      : dx_1 < dx_2
	      );
    });
}

// return the current blocks in range sorted by distance from the light
std::vector<std::shared_ptr<Block>> const Light::getBlocks()
{
  std::vector<std::shared_ptr<Block>> res;

  for(auto& block : m_blocks)
    {
      if( isInRange(block->sprite) ) res.push_back(block);
    }
  sort(res);
  return res;
}

Light::~Light()
{
  
}
