#include <LightManager.hpp>
#include <cassert>
#include <Light.hpp>
#include <algorithm>

// to get M_PI
#define _USE_MATH_DEFINES
#include <math.h>
//
sf::Color const LightManager::m_minimalColor = sf::Color(123,123,123);

LightManager::LightManager( std::shared_ptr<sf::RenderWindow> window ) :
  m_window(window)
  , m_circlePart(120) // 60 bit too low
  , m_rayDivision(100)
  , m_triangles(sf::Triangles)
  // , m_triangles(sf::LinesStrip)
{
  
}

// add a light to the manager
void LightManager::addLight(float x, float y, sf::Color color, float intensity, float radius)
{
  assert(x >= 0);
  assert(x < m_window->getSize().x );
  assert(y >= 0);
  assert(y < m_window->getSize().y );

  m_lights.emplace_back( new Light(this, x, y, intensity, radius, color) );
}

// add a block sprite
void LightManager::addBlock(std::shared_ptr<sf::Sprite>& sp, bool moving,  BLOCK_STATUS status, sf::Color col)
{
  for ( auto& light : m_lights )
    {
      light->addBlock(sp, moving, status, col);
    }
}

// add a block with warning
void LightManager::addBlock(sf::Sprite* sp, bool moving, BLOCK_STATUS status, sf::Color col)
{
  assert(sp);
  std::cout << " [LightManager] Warning: creating a shared_ptr from a raw ptr may lead to a segmentation fault. " << std::endl;
  std::shared_ptr<sf::Sprite> sprite(sp);  

  for ( auto& light : m_lights )
    {
      light->addBlock(sprite, moving,  status, col);
    }
}

// add a block with warning
void LightManager::addBlock(sf::Sprite& sp, bool moving, BLOCK_STATUS status, sf::Color col)
{
  std::cout << " [LightManager] Warning: creating a shared_ptr from a lvalue reference may lead to a segmentation fault. " << std::endl;
  std::shared_ptr<sf::Sprite> sprite(&sp);

  for ( auto& light : m_lights )
    {
      light->addBlock(sprite, moving, status, col);
    }
}

// Strict containement, do not colorize the sprite
bool LightManager::contains(std::shared_ptr<sf::Sprite>& sp, float x, float y)
{
  float sp_x = sp->getPosition().x;
  float sp_y = sp->getPosition().y;
  float sp_w = sp->getTextureRect().width * sp->getScale().x;
  float sp_h = sp->getTextureRect().height* sp->getScale().y;
  
  return ( x >= sp_x && x <= sp_x + sp_w && y >= sp_y && y <= sp_y + sp_h );
}

// Half containement, returns true once the sprite is halfed colorized
bool LightManager::containsHalf(std::shared_ptr<sf::Sprite>& sp, float x, float y)
{
  float sp_x = sp->getPosition().x;
  float sp_y = sp->getPosition().y;
  float sp_w = sp->getTextureRect().width * sp->getScale().x;
  float sp_h = sp->getTextureRect().height* sp->getScale().y;
  float center_x = sp_x + sp_w / 2.f;
  float center_y = sp_y + sp_h / 2.f;

  return  fabs(center_x - x) < sp_w /4.f && fabs(center_y - y) < sp_h /4.f;
 
}

void LightManager::updateRayTriangle(RayTriangle& triangle, std::shared_ptr<Light>& light)
{
  updateRayTriangleFromBlocks(triangle, light);
  // update from other types
  // ...
}

/// Modify position regarding blocks 
void LightManager::updateRayTriangleFromBlocks(RayTriangle& triangle, std::shared_ptr<Light>& light)
{
  // DATAS
  float light_x = light->getX();
  float light_y = light->getY();
  float light_radius = light->getRadius();
  float np_x = triangle.newPoint.position.x;
  float np_y = triangle.newPoint.position.y;
  int direction_x = (np_x > light_x) ? -1 : 1;
  int direction_y = (np_y > light_y) ? -1 : 1;
    
  auto& blocks = light->getBlocks();
  for (auto& block : blocks )
    {
      auto& sprite =  block->sprite;
      float sp_x = sprite->getPosition().x;
      float sp_y = sprite->getPosition().y;
      float sp_w = sprite->getTextureRect().width * sprite->getScale().x;
      float sp_h = sprite->getTextureRect().height* sprite->getScale().y;

      // to opti (actually won 500 FPS instead of a true condition and a single block)
      if (
	  // if sprite and triangle on the same half-vertical-disk
	  (( direction_x == 1 && sp_x < light_x  )
	   || ( direction_x == -1 && sp_x + sp_w > light_x  ))
	  &&
	  (
	   // if sprite and triangle on the same half-horizontal-disk
	   (( direction_y == 1 && sp_y < light_y  )
	    || ( direction_y == -1 && sp_y + sp_h > light_y  ))
	    )
	  )
	{
	  float testX = light_x;
	  float testY = light_y;
	  float xLength = triangle.newPoint.position.x - light_x;
	  float yLength = triangle.newPoint.position.y - light_y ;

	  // let's go step by step to the sprite
	  for ( size_t i(0) ; i < m_rayDivision ; ++i )
	    {
	      if ( contains(sprite, testX, testY) )
		{
		  switch(block->status)
		    {
		    case BLOCK_STATUS::OPAQUE :
		      {
			// show some black :/
			// testX -= xLength / m_rayDivision;
			// testY -= yLength / m_rayDivision;
			triangle.newPoint.position = sf::Vector2f(testX, testY);
			break;
		      }
		      break;

		    case BLOCK_STATUS::TEINTED :
		      {
			triangle.newPoint.color = block->color;
			triangle.lastPoint.color = block->color;
		      }
		      break;

		    default : case BLOCK_STATUS::TRANSPARENT :
		      {
			
		      }
		      break;
		    }
		}
	      else
		{
		  testX += xLength / m_rayDivision;
		  testY += yLength / m_rayDivision;
		}
	    }
	}
    }
}

void LightManager::colorizeRayTriangle(RayTriangle& triangle, std::shared_ptr<Light>& light)
{
  // COLORIZE
  // Origin has the light color
  triangle.origin.color *= getColor(light, triangle.origin.position.x, triangle.origin.position.y);

  // New point has a color depending of objects
  triangle.newPoint.color *= getColor(light, triangle.newPoint.position.x, triangle.newPoint.position.y);

  // last already has a color, but to be sure we put it here
  triangle.lastPoint.color *= getColor(light, triangle.lastPoint.position.x, triangle.lastPoint.position.y);

}

sf::Color LightManager::getColor(std::shared_ptr<Light>& light, float distance)
{
  size_t r = light->getColor().r;
  size_t g = light->getColor().g;
  size_t b = light->getColor().b;
  float light_radius = light->getRadius();
  float light_intensity = light->getIntensity();
  
  return sf::Color(r,g,b, 255 * light_intensity * ( 1 - distance / light_radius ) );
}

sf::Color LightManager::getColor(std::shared_ptr<Light>& light, float x, float y)
{
  float light_x = light->getX();
  float light_y = light->getY();
    
  float dx = fabs(x - light_x);
  float dy = fabs(y - light_y);

  float avgDistance =  sqrt(dx*dx + dy*dy);

  return getColor(light, avgDistance);

}

// Draw the light with all their interactivity
void LightManager::drawTheLigths(void)
{
  // Every lights
  for( auto& light : m_lights )
    {
      // DATAS
      float light_x = light->getX() ; 
      float light_y = light->getY() ;
      float light_radius = light->getRadius();

      // For every circle division (circlePart)
      for ( size_t j(0); j <= m_circlePart ; ++j )
	{
	  float lastAngle = (float)((j == 0) ? 0 : j-1) * (2.f * M_PI / (float)m_circlePart); // basic trigo in radians
	  float angle = (float)j * (2.f * M_PI / (float)m_circlePart);
	  RayTriangle triangle;
	  
	  // origin
	  triangle.origin = sf::Vector2f(light_x, light_y);

	  // POSITION WITH NO OBJECT
	  // New point
	  float new_x = light_x + light_radius * std::cos(M_PI * 0.5 - angle); // basic trigo
	  float new_y = light_y + light_radius * std::sin(M_PI * 0.5 - angle); // basic trigo
	  triangle.newPoint = sf::Vector2f( new_x, new_y );

	  // Precedent point
	  if ( m_triangles.getVertexCount() >= 3 )
	    // copy of precedent point
	    {
	      triangle.lastPoint =  m_triangles[ m_triangles.getVertexCount() - 2].position;
	    }
	  else
	    // first point
	    {
	      float last_x = light_x + light_radius * std::cos(M_PI * 0.5 - lastAngle);
	      float last_y = light_y + light_radius * std::sin(M_PI * 0.5 - lastAngle);
	      triangle.lastPoint = sf::Vector2f( last_x, last_y);
	    }

	  updateRayTriangle(triangle, light);	      
	  
	  // Add the vertex
	  colorizeRayTriangle(triangle, light);
	  m_triangles.append(triangle.toVertex(0));
	  m_triangles.append(triangle.toVertex(1));
	  m_triangles.append(triangle.toVertex(2));
	}
      draw();
    }
}


void LightManager::draw(void)
{
  m_window->draw(m_triangles);
  m_triangles.clear();
}

void LightManager::update( float dt )
{
  drawTheLigths();
}

LightManager::~LightManager()
{
  
}
